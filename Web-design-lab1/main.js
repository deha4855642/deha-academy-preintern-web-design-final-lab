// Modal Image Gallery
function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
    var captionText = document.getElementById("caption");
    captionText.innerHTML = element.alt;
  }
  
  
  // Toggle between showing and hiding the sidebar when clicking the menu icon
  var mySidebar = document.getElementById("mySidebar");
  
  function w3_open() {
    if (mySidebar.style.display === 'block') {
      mySidebar.style.display = 'none';
    } else {
      mySidebar.style.display = 'block';
    }
  }
  
  // Close the sidebar with the close button
  function w3_close() {
      mySidebar.style.display = "";
  }

  function w3_signIn(){
    // Redirect to the signupBasic.html page in the same directory
    window.location.href ='signin.html';
  }

  function redirectToSignupPage1() {
    // Redirect to the signupBasic.html page in the same directory
    window.location.href = 'signupBasic.html';
}
function redirectToSignupPage2() {
  // Redirect to the signupBasic.html page in the same directory
  window.location.href = 'signupPro.html';
}
function redirectToSignupPage3() {
  // Redirect to the signupBasic.html page in the same directory
  window.location.href = 'signupPre.html';
}
// Validate the contact form
function validateForm() {
  var name = document.forms["contactForm"]["Name"].value;
  var email = document.forms["contactForm"]["Email"].value;
  var subject = document.forms["contactForm"]["Subject"].value;
  var message = document.forms["contactForm"]["Message"].value;

  if (name === "" || email === "" || subject === "" || message === "") {
    alert("All fields must be filled out");
    return false;
  }

  if (!validateEmail(email)) {
    alert("Invalid email address");
    return false;
  }
  return true;
}

// Validate email format
function validateEmail(email) {
  var emailPattern = /\S+@\S+\.\S+/;
  return emailPattern.test(email);
}


function successSendMessage(){
  if (validateForm()) {
  alert("Message sent successfully!");
  window.location.href = 'index.html';
  }
}